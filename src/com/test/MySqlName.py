import pymysql
import xlwt
# 数据库操作
# create table my_name(
#    id INT NOT NULL AUTO_INCREMENT,
#    keyword VARCHAR(100) NOT NULL,
#    myexplain TEXT,
#    PRIMARY KEY (`id`)
# )ENGINE=InnoDB DEFAULT CHARSET=utf8
config = {
    "host": "61.164.246.80",
    "user": "sq_shouhulife",
    "password": "itd347",
    "database": "sq_shouhulife",
    "charset": "utf8"
}
conn = pymysql.connect(**config)


# 添加数据
def save_data(list_name):
    cursor = conn.cursor()
    sql = "INSERT INTO my_name(keyword,myexplain) VALUES(%s,%s)"
    cursor.executemany(sql, list_name)
    conn.commit()  # 提交数据
    cursor.close()


# 查询表里所有数据
def find_all():
    cursor = conn.cursor()
    query = ('select * from my_name')
    cursor.execute(query)
    tab_list = []
    for (id, keyword, myexplain) in cursor:
        my_map = {'id': id, 'keyword': keyword, 'myexplain': myexplain}
        tab_list.append(my_map)
    cursor.close()
    return tab_list


# 查询表里所有数据
def find_by_key(key):
    cursor = conn.cursor()
    query = ('select * from my_name where keyword=%s')
    cursor.execute(query, key)
    tab_list = []
    for (id, keyword, myexplain) in cursor:
        my_map = {'id': id, 'keyword': keyword, 'myexplain': myexplain}
        tab_list.append(my_map)
        # print(id, keyword, myexplain)
    cursor.close()
    return tab_list


# 从文件中读取数据
def my_red(path):
    lines = []
    with open(path) as f:
        line = f.readline()
        while line:
            line = line.strip()
            if len(line) != 0:
                # print(line)
                k = line[:line.find('：')]
                v = line[line.find('：')+1:]
                map1 = {'keyword': k, 'myexplain': v}
                lines.append(map1)
            line = f.readline()
    return lines


# 读取txt文件的列表保存到数据库
def save_all(isrun):
    list_all = my_red('f:/namelist.txt')
    print("文件中共有 {} 字".format(len(list_all)))
    my_int = 0
    for my_map in list_all:
        list_t = find_by_key(my_map.get('keyword'))
        if len(list_t) == 0:
            my_int += 1
            my_data = (my_map.get('keyword'), my_map.get('myexplain'))
            list_name = [my_data]
            # print(list_name)
            if isrun:
                save_data(list_name)
            print('保存 {} 成功, 第 {} 个'.format(my_map.get('keyword'), str(my_int)))
        else:
            print('文件中key: {}={},数据库中key: {}'.format(my_map.get('keyword'), my_map.get('myexplain'), list_t))


# 打印表里面的所有数据
def print_tab():
    mylist = find_all()
    row = 0
    row1 = 0
    # for row in range(0, len(mylist)):
    #     for row1 in range(0, len(mylist)):
    #         print('张 {} {}'.format(mylist[row]['keyword'], mylist[row1]['keyword']))
    # 打印表里所有数据
    for data in mylist:
        print('   {} :{}  \n'.format(data['keyword'], data['myexplain']))
    print('数据库中总条数：{} ,总共有{}个名字'.format(len(mylist), row*row1))


def test_excel():
    # 创建一个workbook 设置编码
    workbook = xlwt.Workbook(encoding='utf-8')
    # 创建一个worksheet
    worksheet = workbook.add_sheet('My Worksheet', cell_overwrite_ok=True)
    # 写入excel
    # 参数对应 行, 列, 值
    # worksheet.write(2, 0, label='this is test0000')
    mylist = find_all()
    row = 1
    for row in range(0, len(mylist)):
        # print(mylist[row]['id'])
        worksheet.write(row, 0, label=mylist[row]['id'])
        worksheet.write(row, 1, label=mylist[row]['keyword'])
        worksheet.write(row, 2, label=mylist[row]['myexplain'])
    # 保存
    workbook.save('Excel_test.xls')


if __name__ == '__main__':
    print(print_tab())
    # save_data(list_name)
    # print(my_red('f:/namelist.txt'))
    # print(find_by_key('玥'))
    # save_all(False)

    # test_excel()
    conn.close()
    # 张姝彤  张佳颖  张玥馨  张诗敏

    # 张廷初   张亮   张毅  张彦  张昊哲   张玄良  张鼎哲  张鼎
    # 亮     意为光明、清楚、明朗。可起名为：晶亮、启亮、广亮。义利分明，多才能干，有爱情厄，成功隆昌。
    # 廷     本意指封建时代君主受朝问政的地方，引申为尊贵、高尚、聪明。很适合男生起名用字。寓意着男孩子具有高尚的品格，能力强，受人爱戴的意思。
    # 初     指事物的开端，用作人名时，意为地位崇高，身份尊贵，有丰富的涵养，待人温和，彬彬有礼，展露锋芒的含义。
    # 昊     大，广大无边。常用于指天。
    # 哲     哲字的意思主要是聪明有才能，此外也指聪明有才能的人。
    # 毅     毅指意志坚定、果断，同时也是男性所面具有的一种品德。
    # 彦     旧时指有才德的杰出人物，象征有才学，操守廉政，名利双收，成功昌隆。
    # 蒂    :15画。清雅多才，秀气贤能，中年吉祥，晚年隆昌幸福。
    # 良    :善良、优良。以“良”字入名的如：万梓良、张学良、何良俊、吴子良等。
    # 玄    :性格复杂，晚年吉祥，子孙繁荣，官运旺。
    # 鼎    :意为正当，大，庄重。可起名为：鼎铭、鼎力、鼎盛、鼎昌、鼎元。精明公正，智勇双全，官运成功隆昌，出国荣贵之字。
