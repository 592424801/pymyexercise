import os


def down():
    print("-======================开始执行====================")
    # 现在关机 ：shutdown -s -t 0
    # os.system('shutdown -s -t ' + str(0))
    os.system('shutdown -s -t ' + str(60*60*1))
    # 现在注销
    # os.system('shutdown /l /t 0')
    # shutdown -a (取消关机，重启，注销等指令)
    # os.system('shutdown -a')


if __name__ == '__main__':
    down()