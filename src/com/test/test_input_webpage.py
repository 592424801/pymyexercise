from selenium import webdriver
import time
from selenium.webdriver.common.by import By
import tkinter as tk
# 导入消息对话框子模块
import tkinter.messagebox
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# 打开浏览器
# driver = webdriver.Chrome()
driver = webdriver.Ie()


# 这是一个浏览器自动化测试脚本。 可以实现自动登录操作。
# 打开浏览器
def deicide_task():
    try:
        global entry
        # 设置浏览器大小：全屏
        # driver.maximize_window()
        # 设置分辨率 500*500
        # driver.set_window_size(500, 500)
        # 打开网页
        url = str(entry.get())
        print('---------------:'+url)
        if url is None or len(url) == 0:
            url = 'https://htgl.xiaolulive.cn/'
        if not url.startswith("http"):
            url = 'https://' + str(url)
        driver.get(url)
    except Exception as e:
        print('出错了你好..' + str(e))

    # 关闭浏览器
    # driver.quit()


# 刷新浏览器页面
def deicide_refresh():
    driver.refresh()

# ========================================================================================================业务逻辑
# 测试百度搜索指定内容
def deicide_baidu_search():
    try:
        global driver
        # 找到表单元素并填写信息
        input_element = driver.find_element(By.ID, "kw")
        input_element.send_keys("python 自动化测试")
        time.sleep(1)
        # 提交表单
        driver.find_element(By.ID, "su").click()
    except Exception as e:
        deicide_msg("执行出错" + str(e))


# 小鹿后台登录
def deicide_xiaolu_login():
    try:
        deicide_output_log('输入用户账号信息点击登录')
        global driver
        # 找到表单元素并填写信息
        input_element = driver.find_element(By.NAME, "username")
        input_element.send_keys("zhanghong")
        # time.sleep(1)
        input_element1 = driver.find_element(By.NAME, "password")
        input_element1.send_keys("xuhuan.59")
        # time.sleep(1)
        # 提交表单
        wait = WebDriverWait(driver, 10)
        button = wait.until(EC.presence_of_element_located((By.CLASS_NAME, "el-button--primary")))
        # 点击按钮
        button.click()
    except Exception as e:
        print('出错--'+str(e))


# 导航到用户管理
def deicide_to_user_manager():
    deicide_output_log('点击菜单进入角色管理')
    # hamburger-container el-submenu__title
    msg = ["菜单", (By.XPATH, '//div[@class="el-submenu__title"]')]
    msg1 = ["菜单1", (By.XPATH, '//li[@class="el-menu-item"]')]
    try:
        deicide_output_log('点击系统设置')
        driver.find_element(By.ID, "hamburger-container").click()
        cll = driver.find_elements(By.XPATH, msg[1][1])
        cll[0].click()
        time.sleep(2)
        deicide_output_log('点击角色管理')
        cll1 = driver.find_elements(By.XPATH, msg1[1][1])
        cll1[0].click()
    except Exception as e:
        print("出错{0}".format(str(e)))

# 测试点击按钮
def deicide_click_btn():
    deicide_output_log('开始执行登录操作')
    deicide_xiaolu_login()
    time.sleep(2)
    deicide_output_log('今日首页')
    deicide_to_user_manager()
    time.sleep(2)
    try:
        # 等待元素加载完成
        wait = WebDriverWait(driver, 20)
        # button = wait.until(EC.presence_of_element_located((By.CLASS_NAME, "el-button--primary")))
        # # 点击按钮
        # button.click()

        msg = ["编辑", (By.XPATH, '//button[@class="el-button el-button--primary el-button--small"]')]
        num = 2
        try:
            deicide_output_log('点击第三行的编辑按钮')
            # btns = wait.until(EC.presence_of_element_located(msg[1]))
            # btns.click()
            #
            cll = driver.find_elements(By.XPATH, msg[1][1])
            cll[num].click()
            print("成功点击{0}".format(msg[0]))
        except Exception as a1:
            print("没有找到{0}{1}".format(msg[0], str(a1)))
            # driver.quit()
    except Exception as e:
        print(str(e))
# =========================================================================================================END


# 创建窗口
window = tk.Tk()
entry = tk.Entry(window)
# text(多行文本)
text = tk.Text(window, width=50, height=50)
def show_main_ui():
    window.geometry('400x500')
    window.resizable(0, 0)
    window.title("deicide自动化辅助工具")

    lb = tk.Label(text='网页信息录入', font=('times', 15, 'bold'), fg='#CD7054')
    lb.grid(row=0, column=2, columnspan=4)

    # 添加标签和输入框
    label = tk.Label(window, text="网页地址")
    label.grid(row=1, column=0, columnspan=1, padx=(10, 0), pady=(10, 10))
    entry.grid(row=1, column=2, columnspan=1, padx=(10, 0), pady=(10, 10))

    # 添加按钮
    button = tk.Button(window, text="打开这个网页", command=deicide_task)
    button.grid(row=1, column=3, columnspan=1, padx=(10, 0), pady=(10, 10))

    # 添加按钮
    button1 = tk.Button(window, text="刷新页面", command=deicide_refresh)
    button1.grid(row=1, column=4, columnspan=1, padx=(10, 0), pady=(10, 10))
    # 添加按钮
    button1 = tk.Button(window, text="搜索", command=deicide_click_btn)
    button1.grid(row=2, column=2, columnspan=4, padx=(10, 0), pady=(10, 10))

    # 添加一个多行文本显示输出信息
    text.grid(row=3, column=0, columnspan=5)
    # 运行窗口
    window.mainloop()


# 消息提示框
def deicide_msg(msg):
    # 弹出对话框
    result = tkinter.messagebox.showinfo(title='温馨提示', message=msg)
    # 返回值为：True或者False
    print(result)


# 输出操作日志
def deicide_output_log(var):
    text.insert('end', '\r\n{0}'.format(var))


if __name__ == '__main__':
    # str1 = driver.capabilities['browserVersion']  # 查看chrome版本
    # str2 = driver.capabilities['chrome']['chromedriverVersion'].split(' ')[0]  # 查看python下的chromedriver版本
    # print(str1)
    # print(str2)
    show_main_ui()