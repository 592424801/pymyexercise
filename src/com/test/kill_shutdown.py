import tkinter as tk
import os

def set_shutdown_time():
    new_time = int(entry.get()) * 60
    os.system(f"shutdown /s /t {new_time}")

# 创建窗口
window = tk.Tk()
window.title("设置系统关闭时间")

# 添加标签和输入框
label = tk.Label(window, text="请输入新的系统关闭时间（单位：分钟）：")
label.pack()
entry = tk.Entry(window)
entry.pack()

# 添加按钮
button = tk.Button(window, text="设置关闭时间", command=set_shutdown_time)
button.pack()

# 运行窗口
window.mainloop()