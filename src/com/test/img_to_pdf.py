import img2pdf
import os

# 将图片转换成pdf， 多张图片转换


def task():
    img_path = r'C:\Users\Administrator\Desktop\video'
    imgs = []
    for f in os.listdir(img_path):
        if not f.endswith('.png'):
            continue
        path = os.path.join(img_path, f)
        if os.path.isdir(path):
            continue
        imgs.append(path)
        with open(img_path+'\\pdf\\' + 'name.pdf', 'wb') as f_pdf:
            f_pdf.write(img2pdf.convert(imgs))
            # convert  里面可以是图片数组，也可以是单张图片


if __name__ == '__main__':
    task()