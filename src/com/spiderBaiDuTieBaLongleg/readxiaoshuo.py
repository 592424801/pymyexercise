import requests
from bs4 import BeautifulSoup


# 元尊
url_yuan_zun = 'https://www.biqugex.com/book_139/47400246.html'
# 沧元图
url_cang_yuan_tu = 'https://www.biqugex.com/book_101159/493778652.html'


# 获取服务器的页面数据
def get_index(url):
    try:
        respose = requests.get(url)
        respose.encoding = 'gbk'
        if respose.status_code == 200:
            return respose.text
    except Exception as e:
        print('获取网页数据异常：'+str(e))
        return None


def get_info_txt():
    # text = get_index(url_yuan_zun)

    text = get_index(url_cang_yuan_tu)
    try:
        soup = BeautifulSoup(text, "html.parser")
        pid = soup.findAll('div', {'class': 'showtxt'})
        print(len(pid))
        wenzang = str(pid[0])
        list_info = wenzang.split('<br/>')
        for t in list_info:
            input("  ")
            if len(t) > 60:
                k = len(t) / 60
                # print(k)
                jb = 0
                while(jb < k):
                    print(t[jb*60:jb*60+60])
                    jb += 1
            else:
                print(t)
    except Exception as e:
        print('error' + str(e))


if __name__ == '__main__':
    get_info_txt()
