'''
获取贴吧里面的所有图片
先判断总共有多少个子页面，然后依次爬取每个页面的所有图片
'''

import requests
from bs4 import BeautifulSoup
import spiderUrl


def analyze_page(url):
    myLink = []
    title_name = ''
    try:
        text = spiderUrl.get_index(url)
        soup = BeautifulSoup(text, "html.parser")
        pid = soup.findAll('div', {'id': 'j_core_title_wrap'})
        title_name = pid[0].findAll('h3')[0].get('title')
        page_sizestr = soup.findAll('li', {'class': 'l_reply_num'})
        page_size = page_sizestr[0].findAll('span')[1].text
        myLink = GetPageImgUrl(text)
        if int(page_size) > 1:
            print(page_size)
            for page in range(2, int(page_size)):
                myLink.extend(GetPageImgUrl(spiderUrl.get_index(url+'?pn='+str(page))))
        # print(myLink)
    except Exception as e:
        print('analyze error:'+str(e))
    return myLink, title_name


# 获取页面的所有图片连接
def GetPageImgUrl(text):
    LinkListImg = []
    try:
        soup = BeautifulSoup(text, "html.parser")
        pid = soup.findAll('img', {'class': 'BDE_Image'})
        print('page image size:%d', len(pid))
        for aa in pid:
            href = aa.get('src')
            if href not in LinkListImg:
                LinkListImg.append(href)
            # print(LinkList)
    except Exception as e:
        print('error:' + str(e))
    return LinkListImg
