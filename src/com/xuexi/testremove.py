import pygame
from PIL import Image

# 加载图片
img = Image.open("example.png")

# 获取图片尺寸
img_width, img_height = img.size

# 初始化Pygame
pygame.init()
screen = pygame.display.set_mode((img_width, img_height))

# 将图片转换为Pygame Surface对象
pygame_img = pygame.image.fromstring(img.tobytes(), img.size, img.mode)

# 设置图片的起始坐标
x = 0
y = 0

# 游戏循环
while True:
    # 处理事件
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()

    # 清屏
    screen.fill((255, 255, 255))

    # 绘制图片
    screen.blit(pygame_img, (x, y))

    # 移动图片
    x += 1

    # 更新屏幕
    pygame.display.update()
