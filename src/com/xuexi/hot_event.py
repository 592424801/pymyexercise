import requests
from bs4 import BeautifulSoup
import datetime

def getHot():
    # 获取当前日期
    today = datetime.date.today().strftime('%Y-%m-%d')

    # 设置Google搜索的URL地址
    url = f'https://www.google.com/search?q={today}+热点'

    print(url)

    # 设置请求头部信息
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36'
    }

    # 发送请求并获取响应内容
    response = requests.get(url, headers=headers)

    # 使用BeautifulSoup解析HTML页面
    soup = BeautifulSoup(response.text, 'html.parser')

    # 找到所有搜索结果的标题和摘要
    results = soup.select('.BNeawe.s3v9rd.AP7Wnd')

    # 遍历搜索结果并输出标题和摘要
    for i in range(0, len(results), 2):
        title = results[i].get_text().strip()
        abstract = results[i + 1].get_text().strip()
        print(title)
        print(abstract)
        print('-' * 50)


if __name__ == "__main__":
    getHot()
