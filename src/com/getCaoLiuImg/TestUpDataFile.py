
import requests

upload_url = 'http://118.123.159.19:81/hrzy/pub/fileUpAndDown/fileUploads'
# header = {"ct": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"}
# 此处是重点！我们操作文件上传的时候，把目标文件以open打开，然后存储到变量file里面存到一个字典里面
files = {'file': open('D:\\timg.jpg', 'rb')}
upload_data = {"businessId": "402881ff69b8c8430169b91c46a60116", "childType": "lrshjl"}

# 此处是重点！我们操作文件上传的时候，接口请求参数直接存到upload_data变量里面，在请求的时候，直接作为数据传递过去
upload_res = requests.post(upload_url, upload_data, files=files)
print(upload_res.text)

