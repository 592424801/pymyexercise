import requests
import pymysql
import json
import _thread
import time
import hashlib
import random

# ======================================================================================================================
# readme
# 操作轻轻的后台管理接口
# 1.可以批量注册轻轻的账号
# 2.可以实现修改轻轻的用户账号类型      批量实名认证方便开播
# 3.可以下载轻轻的用户数据
#

# api_base_url = "http://api.qingqingzhibo.com/api/"
# base_url = "http://htgl.qingqingzhibo.com/api/"  # 332501198011270427
# Authorization = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVc2VySWQiOjEyNiwiVXNlck5hbWUiOiJ6aGFuZ2hvbmciLCJOYW1lIjoi5byg5rSqIiwiUGFyZW50SWQiOjAsIlBhcmVudE5hbWUiOiIiLCJCdXNpbmVzc0ZlZSI6MCwiRmVlIjowLCJSb2xlcyI6WzE2XSwiUm9sZU5hbWUiOiLlvIDlj5HkurrlkZgiLCJJc0FkbWluIjpmYWxzZSwiRXhwaXJ5RGF0ZVRpbWUiOiIyMDIyLTAyLTIxVDExOjEzOjQwLjI0NTg4MzMrMDg6MDAifQ.-F9h43IfE6tC6sytEYpzJz4DPso1wqDEDfBp6Eb30cU'
# Cookie = 'Admin-Token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVc2VySWQiOjEyNiwiVXNlck5hbWUiOiJ6aGFuZ2hvbmciLCJOYW1lIjoi5byg5rSqIiwiUGFyZW50SWQiOjAsIlBhcmVudE5hbWUiOiIiLCJCdXNpbmVzc0ZlZSI6MCwiRmVlIjowLCJSb2xlcyI6WzE2XSwiUm9sZU5hbWUiOiLlvIDlj5HkurrlkZgiLCJJc0FkbWluIjpmYWxzZSwiRXhwaXJ5RGF0ZVRpbWUiOiIyMDIyLTAyLTIxVDExOjEzOjQwLjI0NTg4MzMrMDg6MDAifQ.-F9h43IfE6tC6sytEYpzJz4DPso1wqDEDfBp6Eb30cU; sidebarStatus=0'
# Host = 'htgl.qingqingzhibo.com'

api_base_url = "http://apitest.qingqingzhibo.com/api/"
base_url = "http://htgltest.qingqingzhibo.com/api/"  # 332501198011270427
Authorization = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVc2VySWQiOjEyNiwiVXNlck5hbWUiOiJ6aGFuZ2hvbmciLCJOYW1lIjoi5byg5rSqIiwiUGFyZW50SWQiOjAsIlBhcmVudE5hbWUiOiIiLCJCdXNpbmVzc0ZlZSI6MCwiRmVlIjowLCJSb2xlcyI6WzE2XSwiUm9sZU5hbWUiOiLlvIDlj5HkurrlkZgiLCJJc0FkbWluIjpmYWxzZSwiRXhwaXJ5RGF0ZVRpbWUiOiIyMDIyLTAzLTIyVDE4OjU0OjUyLjY5ODc1NDYrMDg6MDAifQ.N30KnGXTHuTzVGBwFF95uis5xxkSmMpvgdO4vAmhsrA'
Cookie = 'Admin-Token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVc2VySWQiOjEyNiwiVXNlck5hbWUiOiJ6aGFuZ2hvbmciLCJOYW1lIjoi5byg5rSqIiwiUGFyZW50SWQiOjAsIlBhcmVudE5hbWUiOiIiLCJCdXNpbmVzc0ZlZSI6MCwiRmVlIjowLCJSb2xlcyI6WzE2XSwiUm9sZU5hbWUiOiLlvIDlj5HkurrlkZgiLCJJc0FkbWluIjpmYWxzZSwiRXhwaXJ5RGF0ZVRpbWUiOiIyMDIyLTAzLTIyVDE4OjU0OjUyLjY5ODc1NDYrMDg6MDAifQ.N30KnGXTHuTzVGBwFF95uis5xxkSmMpvgdO4vAmhsrA; sidebarStatus=0'
Host = 'htgltest.qingqingzhibo.com'

TokenFile = "tokenlaimiaoregister.txt"


# Url参数添加验证码
def get_url_yzt(account, password):
    cdt = str(round(time.time() * 1000))
    mydata = account + password + cdt
    yzt = hashlib.md5(mydata.encode(encoding='UTF-8')).hexdigest()
    s_act = account + "_" + password + "_" + yzt
    return "?act="+s_act+"&cdt="+cdt


# 有的接口需要yzt参数
def get_yzt(mydata):
    mydata = mydata + "a4be9f32637e217fb8dbbcae75759fb3"
    return hashlib.md5(mydata.encode(encoding='UTF-8')).hexdigest()


# 登录之后获取用户id
def get_uid():
    file = open(TokenFile, "r")
    fileData = file.read()
    try:
        json_to_python = json.loads(fileData)
        file.close()
        return json_to_python['Data']["uid"]
    except Exception:
        file.close()
        return ""


# 验证码
def get_code(cell):
    text = api_base_url + "yonghu/getloginsms"+"?cell="+cell + "&yzt=" + get_yzt(cell)
    response = get_html(text, "", "get")
    print(response)


# 登录将登录数据放在文件中
def login(phone, code="831246"):  # 831246 202088
    paramss = {"code": code, "cell": phone, "did": "imei866182040064619",
               "mac": "3C:86:D1:5A:EB:13", "dev": "", "plat": "vivo_V1928A", "ver": "28_9",
               "sys": "androidphone", "sver": "1.21.39-test210726_18", "firm": "9",
               "mart": "android", "token": "", "ip": "192.168.0.201", "source": "and-0",
               "yzt": get_yzt(phone), "lat": "30.554378",
               "lng": "104.07132", "city": "510100", "cityname": "成都市",
               "state": "510000", "statename": "四川省"}

    dd = str(paramss)
    par = dd.encode("utf-8").decode("latin-1")
    data = get_html(api_base_url + "yonghu/celllogin", par)
    file = open(TokenFile, "w")
    file.write(data)
    file.close()


# 注册
def register():
    paramss = {"uid": get_uid(), "head": "9be153ce4134d1a19a8c4d52893baaf2/image/default/1628484904720.jpeg",
               "sex": "m", "city": "510100", "state": "510000", "asign": "", "sign": "", "back": "", "zsign": "",
               "share_code": ""}
    text = api_base_url + "yonghu/setuserinfo"
    response = get_html(text, json.dumps(paramss))
    print(response)


# APP端调用API接口html请求
def get_html(url, paramss, type="post"):
    try:
        file = open(TokenFile, "r")
        fileData = file.read()
        token = ""
        yzt = ""
        try:
            json_to_python = json.loads(fileData)
            token = json_to_python['Data']["token"]
            cell = json_to_python['Data']["cell"]
            pwd = json_to_python['Data']["password"]
            yzt = get_url_yzt(cell, pwd)
        except Exception as e1:
            print("===json 解析出错:" + str(e1))
        file.close()
        header = {'Content-Type': 'application/json; charset=UTF-8',
                  "app_type": "1",
                  "version": "1.21.38",
                  "Authorization": token}
        # params:{uid=40076, oid=43296, key=43296, type=0, gid=43, gift_type=1, num=1, plat=android, yzt=56070fae6b37ad772ae6ed994a082038}
        # 代理
        # prox = {}

        if ("post" == type):
            url += yzt
            data = requests.post(url, data=paramss, headers=header, timeout=29)
        else:
            data = requests.get(url, data=paramss, headers=header, timeout=29)
        data.encoding = "utf-8"
        return data.text
    except Exception as e:
        print(str(e))
        return "异常"


# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
#
#                                                下面是后台接口处理
#
# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
# ======================================================================================================================
# 获取吖咪直播所有的用户的用户数据 昵称性别
def get_index1(url, type="post", paramss=""):
    try:
        headers = {
            'Accept': 'application/json, text/plain, */*',
            'Accept - Encoding': 'gzip, deflate',
            'Accept-Language': 'zh-CN,zh;q=0.9',
            'Authorization': Authorization,
            'Cookie': Cookie,
            'Host': Host,
            'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, '
                          'like Gecko) Chrome/90.0.4430.93 Mobile Safari/537.36',
            # 'accept-encoding': 'gzip, deflate, br',
        }
        if "post" == type:
            response = requests.post(url, data=paramss, headers=headers, timeout=39)
        else:
            response = requests.get(url, data=paramss, headers=headers, timeout=39)
        response.encoding = "utf-8"
        if response.status_code == 200:
            return response.text
    except Exception as e2:
        print('获取网页数据异常：' + str(e2))
        return None


def add_db(jsonss, str_msg):
    # 打开数据库连接，不指定数据库
    if isHome == 1:
        conn = pymysql.connect(host='localhost', user='root', password='XUHUAN.59')
    else:
        conn = pymysql.connect('localhost', 'root', 'xuhuan.5130638')
    conn.select_db('laimiao')
    cur = conn.cursor()  # 获取游标
    sunNum = 0
    try:
        # print("===========================================================================================================")
        # print(jsonss)
        jsonStr = json.loads(jsonss)
        for item in jsonStr['Data']['rows']:
            cur.execute("select * from userqq where uid=" + str(item["uid"]))
            # 使用 fetchone() 方法获取一条数据
            data = cur.fetchone()
            # print('添加语句受影响的行数：', data)
            if data is None:
                sql = "insert into userqq (uid,sex,cell,coin,gold_coin,mart,identification_name,identification_no," \
                      "spreaduid,islock,user_level,anchor_level,ip,cdt,plat,sver,register_time) " \
                      "values('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(
                    item["uid"], item["sex"], item["cell"], item["coin"], item["gold_coin"], item["mart"],
                    item["identification_name"], item["identification_no"], item["spreaduid"],
                    item["islock"], item["user_level"], item["anchor_level"], item["ip"], item["cdt"], item["plat"],
                    item["sver"], item["register_time"]
                )
                # print(len(sql))
                sunNum += cur.execute(sql)
            else:
                sql = "update userqq set uid='{}',sex='{}',cell='{}',coin='{}',gold_coin='{}',mart='{}'," \
                      "identification_name='{}'" \
                      ",identification_no='{}',spreaduid='{}',islock='{}',user_level='{}',anchor_level='{}',ip='{}'" \
                      ",cdt='{}',plat='{}',sver='{}',register_time='{}' where uid='{}'".format(
                    item["uid"], item["sex"], item["cell"], item["coin"], item["gold_coin"], item["mart"],
                    item["identification_name"], item["identification_no"], item["spreaduid"],
                    item["islock"], item["user_level"], item["anchor_level"], item["ip"], item["cdt"], item["plat"],
                    item["sver"], item["register_time"], item["uid"]
                )
                sunNum += cur.execute(sql)
            conn.commit()
    finally:
        try:
            cur.close()
            conn.close()
            print(str_msg + '  受影响的行数：' + str(sunNum))
        except Exception as e1:
            print('sql执行成功异常：' + str(e1))


def get_data(name, start, end):
    for i in range(start, end):
        try:
            # http://htgl.cwoqu.com/api/basic/forbidlist?page=1&size=20
            # print("\n\n\n\n\n\n\n\n")
            # print("========================================= 开始获取数据 =================================================")
            str_msg = "                                     " + name + "Task_Page：" + str(i)
            add_db(get_index1(base_url+"users/userlist?page=" + str(i) + "&size=20"), str_msg)
            # print("========================================= 开始获取数据 =================================================")
        except Exception as e1:
            print('循环次数 ' + str(i) + ' 异常：' + str(e1))


isHome = 1  # 公司1 家2


def get_user():
    try:
        num = 0
        xs = 100
        _thread.start_new_thread(get_data, ("Thread-1----", num, num + xs))
        _thread.start_new_thread(get_data, ("Thread-2----", num + xs * 1, num + xs * 2))
        _thread.start_new_thread(get_data, ("Thread-3----", num + xs * 2, num + xs * 3))
        # _thread.start_new_thread(get_data, ("Thread-4----", num + xs * 3, num + xs * 4))
        # _thread.start_new_thread(get_data, ("Thread-5----", num + xs * 4, num + xs * 5))
    except IOError as e:
        print("Error: 无法启动线程")
    while 1:
        pass

# 用户实名认证
def up_data_user_inf(uid, cell):
    paramess = {
        "uid": uid,
        "yuid": uid,
        "user_type": 2,
        "cell": cell,
        "nick": "这是那个的水军呢....",
        "sex": "男",
        "head": "bcb7dcd5566257d753e5ed3ba48043f9/1642739639180.jpg",
        "head_show": "http://yami-imgs.oss-cn-hangzhou.aliyuncs.com/bcb7dcd5566257d753e5ed3ba48043f9/1642739639180.jpg?Expires=1642750450&OSSAccessKeyId=LTAI4GBC6sp7xbYxujJEJmYK&Signature=5f021Jbh%2Bk1Sx1OshuC6EkZ9INc%3D",
        "head_frond": "baf83476cbed828d8caaf67c36ee4cf2/1631012829602.png",
        "head_frond_show": "http://yami-imgs.oss-cn-hangzhou.aliyuncs.com/baf83476cbed828d8caaf67c36ee4cf2/1631012829602.png?Expires=1642750450&OSSAccessKeyId=LTAI4GBC6sp7xbYxujJEJmYK&Signature=Asx6t7sMkeNbxZePtJEKGGZf6Y4%3D",
        "isidentity": 1,
        "identification_name": "王毅",
        "identification_no": "332501198011270427",
        "user_level": 0,
        "anchor_level": 0,
        "jifen_level": 0,
        "sign": "请成为永远疯狂永远浪漫永远清澈的存在"
    }
    print(get_index1(base_url + "users/useredit", "post", paramess))

# 修改用户类型为系统账号
def change_user_type(uid, cell):
    paramess = {
        "uid": uid,
        "yuid": uid,
        "user_type": 2,
        "cell": cell,
        "nick": "过户一个个发",
        "head": "http://yami-imgs.oss-cn-hangzhou.aliyuncs.com/a309765c61761fd56d7f0fb5d7b44b07/1642739554412.jpg?Expires=1642750958&OSSAccessKeyId=LTAI4GBC6sp7xbYxujJEJmYK&Signature=WkQkbQxDFl%2FqSZt6mdVrUKF8jfA%3D",
        "head_frond": "baf83476cbed828d8caaf67c36ee4cf2/1631012829602.png",
        "labour_recharge_fee": 0,
        "labour_fee": 0,
        "invite_fee": 0,
        "profit_fee": 0,
        "store_etime": "0001-01-01 00:00:00",
        "number_etime": "",
        "matchs": 0
    }
    print(get_index1(base_url + "users/updateusershop", "post", paramess))


user_info = {}


# 获取用户uid 和用户cell
def get_user_info():
    for i in range(221, 240):
        phone = "13540124" + str(i)
        text = get_index1(base_url + "users/userlist?page=1&size=20&cell=" + phone, "get")
        try:
            json_to_python = json.loads(text)
            # print(json_to_python['Data']['rows'][0]["uid"])
            # print(json_to_python['Data']['rows'][0]["cell"])
            user_info[phone] = json_to_python['Data']['rows'][0]["uid"]
        except IOError as e:
            print("Error: 无法启动线程")
    print(user_info)


# 查询服务器获取到验证码
def get_phone_code():
    text = get_index1(base_url + "users/yzmlist?page=1&size=20&drange[]=2021-12-31&drange[]=2023-01-27&stime=2021-12-31&etime=2023-01-27", "get")
    # print(text+"--------------------------------------------------------")
    json_to_python = json.loads(text)
    return json_to_python['Data']['rows'][0]["icode"]


# 批量注册用户接口
def register_user(): # 255
    for i in range(222, 242):
        phone = "13540124" + str(i)
        # 1.获取验证码
        get_code(phone)
        # 2.查询发送的验证码
        code = get_phone_code()
        # 3.登录/注册用户保存token 和 uid到本地
        login(phone, code)
        # 4.完善资料
        register()
        # 5.修改用户类型为系统用户
        change_user_type(get_uid(), phone)
        # 6.用户身份证认证
        up_data_user_inf(get_uid(), phone)
        time.sleep(2)


if __name__ == '__main__':
    # 多线程下载用户数据到本地数据库
    # get_user()
    # 获取用户uid 和用户cell 方便下面修改用户的实名认证  修改用户为系统用户
    # get_user_info()
    # 修改用户为实名认证用户
    # up_data_user_inf()
    # 修改用户类型为系统用户
    # change_user_type()

    # 批量注册用户
    register_user()
    print("=============================================================执行完成=======================================")
