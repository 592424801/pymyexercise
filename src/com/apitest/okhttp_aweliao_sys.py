# -*- coding: utf-8 -*-

import requests
import json
import hashlib
import base64

"""

接口测试代买
Created on Tue Jul 27 12:41:39 2021

@author: Administrator
"""

def getHtml(url, paramss, type = "post", token = ""):
    try:
        header = {'Content-Type':'application/json; charset=UTF-8',
            "app_type":"1",
                  "version":"1.21.38-test210617-1",
                  "token": token}  
        # params:{uid=40076, oid=43296, key=43296, type=0, gid=43, gift_type=1, num=1, plat=android, yzt=56070fae6b37ad772ae6ed994a082038}
        # 代理
        # prox = {}
        
        if("post" == type):
            data = requests.post(url,data = paramss, headers=header,timeout=2)
        else:
            data = requests.get(url,data = paramss, headers=header,timeout=2)
        data.encoding = "utf-8"
        # print(data.text)
        return data.text
    except Exception as e:
        print(str(e))
        return "异常"


URLHOST = "http://localhost:8090"  # "http://shouhulife.vicp.cc/" # "http://localhost:8080" #
# ======================================================================================================================
# 后台管理模块
# ======================================================================================================================
def sysLogin():
    paramss = {"username": "admin", "password": "123456"}
    data = getHtml(URLHOST + "/sysadmin/login", json.dumps(paramss))
    print(data)
    try:
        json_to_python = json.loads(data)
        return json_to_python['data']['token']
    except Exception:
        return ""

def sysMenus(token):
    data = getHtml(URLHOST + "/sysadmin/menus", '', 'get', token)
    print(data)

def sysUserList(token):
    paramss = {"query": "", "pagenum": "1", "pagesize": "2"}
    data = getHtml(URLHOST + "/sysadmin/userlist", json.dumps(paramss), 'get', token)
    print(data)

# 主模块
# ======================================================================================================================
if __name__ == '__main__':
    # ======================
    # 后台管理
    token = sysLogin()
    # sysMenus(token)
    sysUserList(token)

