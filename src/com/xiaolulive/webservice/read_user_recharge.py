import xlrd


def read_user_recharge(time):

    excel_path = r'C:\\Users\\Administrator\\Documents\\xiaolu\\'+str(time)+'_用户充值行为分析.xls'
    data = xlrd.open_workbook(excel_path)
    sheet1 = data.sheet_by_index(0)
    row = sheet1.nrows
    list_anchor = []
    for item in range(0, row):
        list_anchor.append(sheet1.row_values(item))

    return list_anchor


if __name__ == '__main__':
    data = read_user_recharge('2023-02-10')

    print(data)
