import xlrd
import datetime


def read_only_recharge(uid):
    excel_path = r'C:\\Users\\Administrator\\Documents\\xiaolu\\'+str(uid)+'_单一用户充值行为分析.xls'
    data = xlrd.open_workbook(excel_path)
    sheet1 = data.sheet_by_index(0)
    row = sheet1.nrows
    list_anchor = []
    for item in range(0, row):
        list_anchor.append(sheet1.row_values(item))
    return list_anchor


if __name__ == '__main__':
    data = read_only_recharge(10001)

    print(data)
