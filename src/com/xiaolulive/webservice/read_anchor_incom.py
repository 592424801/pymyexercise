import xlrd
import datetime


def read_anchor(time):

    excel_path = r'C:\\Users\\Administrator\\Documents\\xiaolu\\'+str(time)+'_主播榜单.xls'
    data = xlrd.open_workbook(excel_path)
    sheet1 = data.sheet_by_index(0)
    sheet2 = data.sheet_by_index(1)
    row = sheet1.nrows
    row1 = sheet2.nrows
    list_anchor = []
    for item in range(0, row):
        list_anchor.append(sheet1.row_values(item))
    for item in range(0, row1):
        list_anchor.append(sheet2.row_values(item))
    return list_anchor


if __name__ == '__main__':
    data = read_anchor('2023-02-10')

    print(data)
