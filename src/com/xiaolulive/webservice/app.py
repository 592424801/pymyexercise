from flask import Flask
from flask import render_template
from flask import request
from read_anchor_incom import *
from read_user_recharge import *
from read_only_anchor_incom import *
from xiaolu_incom_only import *
from read_only_recharge import *
from xiaolu_recharge_only import *
from xiaolu_incom import *
from xiaolu_recharge import *
from xiaolu_luckgift_only import *


app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route('/fenxi')
def hello_fenxi():
    list = []
    # 日期列表显示出来
    for day in range(0, 23):
        time = (datetime.datetime.now() + datetime.timedelta(days=-day)).strftime('%Y-%m-%d')
        try:
            tongji = {}
            data = read_anchor(time)
            data1 = read_user_recharge(time)
            # 统计一下每日的上榜到实际能提现金额，任务提现金额，上榜分成金额
            total_bang = 0.0
            total_task = 0.0
            total_tixian = 0.0
            total_bag = 0.0
            total_color_egg = 0.0
            total_color_egg_cc = 0.0
            total_luck_book = 0.0
            total_luck_book_cc = 0.0
            total_luck_gift = 0.0
            total_recharge = 0.0  # xiaolu_total_chongzhi(time)
            total_yangmao = 0
            renshu_recharge = 0
            renshu_book = 0
            renshu_coloregg = 0
            renshu_gift = 0
            for iindex in range(1, len(data)):
                itemi = data[iindex]
                if itemi[2] is not None:
                    if type(itemi[2]) != str:
                        total_bang += float(itemi[2])
                if itemi[9] is not None:
                    if type(itemi[9]) != str:
                        total_task += float(itemi[9])
                if itemi[12] is not None:
                    if type(itemi[12]) != str:
                        total_tixian += float(itemi[12])
                if itemi[5] is not None:
                    if type(itemi[5]) != str:
                        total_bag += float(itemi[5])
                if itemi[6] is not None:
                    zb = itemi[6]
                    zb = zb.replace('%', '')
                    bili = 0
                    try:
                        bili = float(zb)
                    except Exception as e:
                        print(e)
                    # print(bili)
                    if bili > 90:
                        total_yangmao += 1
            for iindex in range(1, len(data1)):
                itemi = data1[iindex]
                if itemi[3] is not None:
                    if type(itemi[3]) != str:
                        try:
                            if float(itemi[3]) > 0:
                                renshu_book += 1
                        except:
                            print("宝典人数---e")
                if itemi[4] is not None:
                    if type(itemi[4]) != str:
                        total_luck_book_cc += float(itemi[4])
                if itemi[5] is not None:
                    if type(itemi[5]) != str:
                        total_luck_book += float(itemi[5])
                if itemi[6] is not None:
                    if type(itemi[6]) != str:
                        try:
                            if float(itemi[6]) > 0:
                                renshu_coloregg += 1
                        except:
                            print("彩蛋人数---e")
                if itemi[7] is not None:
                    if type(itemi[7]) != str:
                        total_color_egg_cc += float(itemi[7])
                if itemi[8] is not None:
                    if type(itemi[8]) != str:
                        total_color_egg += float(itemi[8])
                if itemi[9] is not None:
                    if type(itemi[9]) != str:
                        try:
                            if float(itemi[9]) > 0:
                                renshu_gift += 1
                        except:
                            print("幸运礼物人数---e")
                if itemi[13] is not None:
                    if type(itemi[13]) != str:
                        total_luck_gift += float(itemi[13])
                if itemi[19] is not None:
                    if type(itemi[19]) != str:
                        total_recharge += float(itemi[19])
                        try:
                            if float(itemi[19]) > 0:
                                renshu_recharge += 1
                        except:
                            print("幸运礼物人数---e")

            # 当天的用户兑换金额
            data_duiyuan = read_user_recharge(time)
            duihuan = 0.0
            if data_duiyuan is not None:
                for item_dh in data_duiyuan:
                    if type(item_dh[20]) == str:
                        continue
                    if item_dh[20] is not None:
                        duihuan += float(item_dh[20])

            tongji['时间'] = time
            tongji['充值金额'] = total_recharge
            tongji['当日兑换'] = round(duihuan, 2)
            tongji['上榜金额'] = round(total_bang / 1000, 2)
            tongji['上榜主播获得'] = round(float(total_bang) * 0.7 / 1000, 2)
            tongji['充值流水奖励'] = "流："+str(round(float(total_bang) * 0.025 / 1000, 1)) +" 充：" \
                               + str(round(float(total_recharge) * 0.03, 1)) + " 共：" \
                               + str(
                round(float(total_recharge) * 0.03 + float(total_bang) * 0.025 / 1000, 1))

            tongji['任务金额'] = round(total_task / 1000, 2)
            ketixian = round((float(total_bang) * 0.7 + float(total_bang) * 0.025 + total_task +
                              float(total_recharge) * 1000 * 0.03) / 1000 - duihuan, 2)
            tongji['可提现金额'] = ketixian
            tongji['实际提现金额'] = total_tixian
            tongji['宝典'] = total_luck_book
            tongji['彩蛋'] = total_color_egg
            tongji['产出背包'] = round(total_color_egg_cc + total_luck_book_cc, 1)
            tongji['背包上榜'] = round(total_bag / 1000, 1)
            tongji['幸运礼物'] = round(total_luck_gift, 1)

            tongji['平台盈亏'] = round(total_recharge - ketixian, 1)
            tongji['背包剩余'] = round((total_color_egg_cc + total_luck_book_cc) - total_bag / 1000, 1)

            tongji['主播数'] = len(data)
            tongji['羊毛数'] = total_yangmao
            tongji['用户数'] = len(data1)
            tongji['充值数'] = renshu_recharge
            tongji['宝典数'] = renshu_book
            tongji['彩蛋数'] = renshu_coloregg
            tongji['幸运数'] = renshu_gift

            list.append(tongji)
        except Exception as e:
            print("报错了" + str(e))
    return render_template('tongji.html', list=list)


@app.route('/incom')
def user_income():
    time = request.values.get("time")
    if time == None:
        time = datetime.datetime.now().strftime('%Y-%m-%d')
    print(time)
    try:
        try:
            tongji = {}
            data = read_anchor(time)
            data1 = read_user_recharge(time)
            # 统计一下每日的上榜到实际能提现金额，任务提现金额，上榜分成金额
            total_bang = 0.0
            total_task = 0.0
            total_tixian = 0.0
            total_bag = 0.0
            total_color_egg = 0.0
            total_color_egg_cc = 0.0
            total_luck_book = 0.0
            total_luck_book_cc = 0.0
            total_luck_gift = 0.0
            total_recharge = 0.0 # xiaolu_total_chongzhi(time)
            total_yangmao = 0
            renshu_recharge = 0
            renshu_book = 0
            renshu_coloregg = 0
            renshu_gift = 0
            for iindex in range(1, len(data)):
                itemi = data[iindex]
                if itemi[2] is not None:
                    if type(itemi[2]) != str:
                        total_bang += float(itemi[2])
                if itemi[9] is not None:
                    if type(itemi[9]) != str:
                        total_task += float(itemi[9])
                if itemi[12] is not None:
                    if type(itemi[12]) != str:
                        total_tixian += float(itemi[12])
                if itemi[5] is not None:
                    if type(itemi[5]) != str:
                        total_bag += float(itemi[5])
                if itemi[6] is not None:
                    zb = itemi[6]
                    zb = zb.replace('%', '')
                    bili = 0
                    try:
                        bili = float(zb)
                    except Exception as e:
                        print(e)
                    # print(bili)
                    if bili > 90:
                        total_yangmao += 1
            for iindex in range(1, len(data1)):
                itemi = data1[iindex]
                if itemi[3] is not None:
                    if type(itemi[3]) != str:
                        try:
                            if float(itemi[3]) > 0:
                                renshu_book += 1
                        except:
                            print("宝典人数---e")
                if itemi[4] is not None:
                    if type(itemi[4]) != str:
                        total_luck_book_cc += float(itemi[4])
                if itemi[5] is not None:
                    if type(itemi[5]) != str:
                        total_luck_book += float(itemi[5])
                if itemi[6] is not None:
                    if type(itemi[6]) != str:
                        try:
                            if float(itemi[6]) > 0:
                                renshu_coloregg += 1
                        except:
                            print("彩蛋人数---e")
                if itemi[7] is not None:
                    if type(itemi[7]) != str:
                        total_color_egg_cc += float(itemi[7])
                if itemi[8] is not None:
                    if type(itemi[8]) != str:
                        total_color_egg += float(itemi[8])
                if itemi[9] is not None:
                    if type(itemi[9]) != str:
                        try:
                            if float(itemi[9]) > 0:
                                renshu_gift += 1
                        except:
                            print("幸运礼物人数---e")
                if itemi[13] is not None:
                    if type(itemi[13]) != str:
                        total_luck_gift += float(itemi[13])
                if itemi[19] is not None:
                    if type(itemi[19]) != str:
                        total_recharge += float(itemi[19])
                        try:
                            if float(itemi[19]) > 0:
                                renshu_recharge += 1
                        except:
                            print("幸运礼物人数---e")

            # 当天的用户兑换金额
            data_duiyuan = read_user_recharge(time)
            duihuan = 0.0
            if data_duiyuan is not None:
                for item_dh in data_duiyuan:
                    if type(item_dh[20]) == str:
                        continue
                    if item_dh[20] is not None:
                        duihuan += float(item_dh[20])

            tongji['充值金额'] = total_recharge
            tongji['当日兑换'] = round(duihuan, 2)
            tongji['上榜金额'] = round(total_bang / 1000, 2)
            tongji['上榜主播获得'] = round(float(total_bang) * 0.7 / 1000, 2)
            tongji['充值流水奖励'] = str(round(float(total_bang) * 0.025 / 1000, 1)) + " " \
                               + str(round(float(total_recharge)*0.03, 1)) + " 共：" \
                               + str(round(float(total_recharge)*0.03, 1)+round(float(total_bang) * 0.025 / 1000, 1))

            tongji['任务金额'] = round(total_task / 1000, 2)
            ketixian = round((float(total_bang) * 0.7 + float(total_bang) * 0.025 + total_task +
                              float(total_recharge)*1000 * 0.03) / 1000 - duihuan, 2)
            tongji['可提现金额'] = ketixian
            tongji['实际提现金额'] = total_tixian
            tongji['宝典'] = total_luck_book
            tongji['彩蛋'] = total_color_egg
            tongji['产出背包'] = round(total_color_egg_cc+total_luck_book_cc, 1)
            tongji['背包上榜'] = round(total_bag/1000, 1)
            tongji['幸运礼物'] = round(total_luck_gift, 1)

            tongji['平台盈亏'] = round(total_recharge - ketixian, 1)
            tongji['背包剩余'] = round((total_color_egg_cc+total_luck_book_cc) - total_bag/1000, 1)

            tongji['主播数'] = len(data)
            tongji['羊毛数'] = total_yangmao
            tongji['用户数'] = len(data1)
            tongji['充值数'] = renshu_recharge
            tongji['宝典数'] = renshu_book
            tongji['彩蛋数'] = renshu_coloregg
            tongji['幸运数'] = renshu_gift

            time_list = []
            # 日期列表显示出来
            for day in range(0, 13):
                time_list.append((datetime.datetime.now() + datetime.timedelta(days=-day)).strftime('%Y-%m-%d'))

            return render_template('user_incom.html', list=data, time_list=time_list, nowtime=time, tongji=tongji)
        except Exception as e:
            print("报错了"+str(e))
            time_list = []
            # 日期列表显示出来
            for day in range(0, 13):
                time_list.append((datetime.datetime.now() + datetime.timedelta(days=-day)).strftime('%Y-%m-%d'))
            return render_template('user_incom.html', time_list=time_list, nowtime=time)

    except:
        time_list = []
        # 日期列表显示出来
        for day in range(0, 13):
            time_list.append((datetime.datetime.now() + datetime.timedelta(days=-day)).strftime('%Y-%m-%d'))
        return render_template('user_incom.html', time_list=time_list, nowtime=time)


@app.route('/user')
def user_recharge():
    time = request.values.get("time")
    if time == None:
        time = datetime.datetime.now().strftime('%Y-%m-%d')
    print(time)
    try:
        data = read_user_recharge(time)

        time_list = []
        # 日期列表显示出来
        for day in range(0, 13):
            time_list.append((datetime.datetime.now() + datetime.timedelta(days=-day)).strftime('%Y-%m-%d'))

        return render_template('user_recharge.html', list=data, time_list=time_list, nowtime=time)
    except:
        time_list = []
        # 日期列表显示出来
        for day in range(0, 13):
            time_list.append((datetime.datetime.now() + datetime.timedelta(days=-day)).strftime('%Y-%m-%d'))
        return render_template('user_recharge.html', time_list=time_list, nowtime=time)


@app.route('/onlyanchor')
def show_post():
    try:
        uid = request.values.get("uid")
        uid = int(float(uid))
        print(uid)
        find_all_only(uid)
        data = read_only_anchor(uid)
        return render_template('only_anchor.html', list=data)
        # return str(uid)+" ---"
    except:
        return render_template('only_anchor.html')


@app.route('/onlyrecharge')
def show_recharge():
    try:
        uid = request.values.get("uid")
        uid = int(float(uid))
        print(uid)
        find_all_recharge(uid)
        data = read_only_recharge(uid)
        return render_template('only_user_record.html', list=data)
        # return str(uid)+" ---"
    except:
        return render_template('only_user_record.html')


@app.route('/onlyuserfenxi')
def show_recharge_fenxi():
    try:
        uid = request.values.get("uid")
        time = request.values.get("time")
        uid = int(float(uid))
        print(uid)
        return fenxi(uid, time)
        # return str(uid)+" ---"
    except Exception as e:
        return "出错了" + str(e)


@app.route('/updata_incom')
def updata_incom():
    try:
        time = request.values.get("time")
        if time == None:
            time = datetime.datetime.now().strftime('%Y-%m-%d')
        print(time)
        xiaolu_incom_task(time)
        return "成功"
        # return str(uid)+" ---"
    except Exception as e:
        return "失败" + str(e)


@app.route('/updata_recharge')
def updata_recharge():
    try:
        time = request.values.get("time")
        if time == None:
            time = datetime.datetime.now().strftime('%Y-%m-%d')
        print(time)
        xiaolu_recharge_task(time)
        return "成功"
        # return str(uid)+" ---"
    except:
        return "失败"


# 判断是否有重复的
def chongfu(list, id):
    for item in list:
        try:
            if type(item) == dict:
                if item['uid'] == id:
                    return False
        except:
            print("---")
    return True


# 获取所有的表
def get_all_excel():
    list_ = []
    for day in range(0, 19):
        time = (datetime.datetime.now() + datetime.timedelta(days=-day)).strftime('%Y-%m-%d')
        data = read_anchor(time)
        list_.append(time)
        for item in data:
            map_item = {}
            zb = item[6]
            zb = zb.replace('%', '')
            bili = 0
            try:
                bili = float(zb)
            except Exception as e:
                print(e)
            # print(bili)
            if bili < 90:
                map_item['uid'] = item[0]
                map_item['nick'] = item[1]
                map_item['bang'] = item[2]
                map_item['zb1'] = item[6]
                map_item['zb2'] = item[7]
                if chongfu(list_, item[0]):
                    list_.append(map_item)

    work_book = xlwt.Workbook(encoding='utf-8')
    sheet = work_book.add_sheet('主播贡献榜')
    sheet.write(0, 0, '用户ID')
    sheet.write(0, 1, '昵称')
    sheet.write(0, 2, '上榜金额')
    sheet.write(0, 3, '直送占比')
    sheet.write(0, 4, '背包占比')
    for index in range(len(list_)):
        if type(list_[index]) == str:
            sheet.write(index + 1, 0, list_[index])
        else:
            sheet.write(index + 1, 0, list_[index]['uid'])
            sheet.write(index + 1, 1, list_[index]['nick'])
            sheet.write(index + 1, 2, list_[index]['bang'])
            sheet.write(index + 1, 3, list_[index]['zb1'])
            sheet.write(index + 1, 4, list_[index]['zb2'])
    work_book.save('C:\\Users\\Administrator\\Documents\\xiaolu\\_主播榜单每日直送占比90.xls')


if __name__ == '__main__':
    # http://192.168.31.71:5000/incom?time=2023-02-13
    app.run(host='0.0.0.0', port=80, debug=True)
    # get_all_excel()