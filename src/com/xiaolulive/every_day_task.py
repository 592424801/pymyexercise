import schedule
from xiaolu_incom import *
from xiaolu_recharge import *


def job_income():
    print('每天晚上11点50执行任务 主播排行')
    time = datetime.datetime.now().strftime('%Y-%m-%d')
    # time = '2023-02-16'
    xiaolu_incom_task(time)


def job_recharge():
    print('每天晚上11点55执行任务 用户充值')
    time = datetime.datetime.now().strftime('%Y-%m-%d')
    # time = '2023-02-16'
    xiaolu_recharge_task(time)


schedule.every().day.at('10:08').do(job_income)
# schedule.every().day.at('10:10').do(job_recharge)
if __name__ == '__main__':
    job_recharge()
    # job_income()
    # while True:
    #     schedule.run_pending()