import pymysql
import datetime
from flask import render_template

# 用户当日玩幸运礼物分析
# 总次数；中奖次数；总送礼金额；总中奖金额；最大间隔中间次数；每种单次金额投入次数和中奖次数；

# 数据库操作
config = {
    "host": "1.117.167.225",
    "user": "root",
    "password": "Aa1234zxcv",
    "database": "qc_live",
    "charset": "utf8"
}


def fenxi(uid, time):
    conn = pymysql.connect(**config)
    dt = datetime.datetime.strptime(time, "%Y-%m-%d")
    time1 = (dt + datetime.timedelta(days=1)).strftime("%Y-%m-%d")
    cursor = conn.cursor()
    tab_list = []
    query = ('select * from t_wallet_bill_202303 where uid = '+str(uid)+' and type=6 and time >= "' + str(
            time) + '" and time<"' + str(time1) + '"')
    cursor.execute(query)
    result = cursor.fetchall()
    for (item) in result:
        item1 = {}
        item1['name'] = item[3]
        item1['cost'] = item[6]
        tab_list.append(item1)

    total_win = 0
    total_cost = 0.0
    total_cost_win = 0.0
    jiangecishu = 0
    max_jg = 0
    for item2 in tab_list:
        cost = item2['cost']
        cost = cost.replace(' 鹿角', '')
        cost = cost.replace('-', '')
        cost = cost.replace('+', '')
        item2['cost'] = float(cost)
        if '幸运礼物收入' == item2['name']:
            if jiangecishu > max_jg:
                max_jg = jiangecishu
            jiangecishu = 0
            total_win += 1
            total_cost_win += float(cost)
        else:
            jiangecishu += 1
            total_cost += float(cost)
    total = len(tab_list)-total_win
    chengben = (total_cost - total_cost_win)/1000
    yinkui = round(total_cost / 10000 - chengben, 2)

    list_gift = {}
    for item2 in tab_list:
        # 每种幸运礼物的送礼情况，送礼礼物名称， 金额， 送礼次数， 中奖次数
        if item2['name'] in list_gift.keys():
            des = list_gift[item2["name"]]
            des["num"] += 1
            des["gailv"] = round(des['win_num']/des['num'], 4)
            list_gift[item2["name"]] = des
        else:
            if item2['name'] == '幸运礼物收入':
                continue
            des = {}
            des["name"] = item2["name"]
            des["cost"] = float(item2["cost"])/1000
            des["num"] = 1
            num_win = 0
            for item3 in tab_list:
                if '幸运礼物收入' == item3['name']:
                    if float(item3["cost"]) == float(item2["cost"]) * 5:
                        num_win += 1
            des["win_num"] = num_win
            des["gailv"] = 0
            list_gift[item2["name"]] = des

    map_gift = {}
    for item2 in tab_list:
        # 每种幸运礼物的送礼情况，送礼礼物名称， 金额， 送礼次数， 中奖次数
        if item2['cost'] in map_gift.keys():
            if item2['name'] == '幸运礼物收入':
                continue
            des = map_gift[item2["cost"]]
            des["num"] += 1
            des["gailv"] = round(des['win_num'] / des['num'], 4)
            map_gift[item2["cost"]] = des
        else:
            if item2['name'] == '幸运礼物收入':
                continue
            des = {}
            # des["name"] = item2["name"]
            des["cost"] = float(item2["cost"]) / 1000
            des["num"] = 1
            num_win = 0
            for item3 in tab_list:
                if '幸运礼物收入' == item3['name']:
                    if float(item3["cost"]) == float(item2["cost"]) * 5:
                        num_win += 1
            des["win_num"] = num_win
            des["gailv"] = 0
            map_gift[item2["cost"]] = des

    # 添加成本和盈亏
    for key in map_gift.keys():
        des = map_gift[key]
        touru = float(des["cost"]) * int(des["num"])
        chanchu = float(des["cost"]) * 5 * int(des["win_num"])
        shangbeng = touru / 10
        chengben1 = round((touru - chanchu), 2)
        des['chengben'] = chengben1
        des['yingkui'] = round(float(shangbeng) - float(chengben1), 2)
        map_gift[key] = des

    # print(list_gift)
    # for kk in list_gift.keys():
    #     print(list_gift[kk])

    # print('总送礼次数：%d; 总中奖次数:%d; 总中奖率:%s; 总投入:%s; 总中奖:%s;成本:%s;盈亏:%s;最大未中次数:%d'
    #       % (total, total_win, round(total_win / total, 4), total_cost, total_cost_win, chengben, yinkui, max_jg))
    gl = 0
    if total > 0:
        gl = round(total_win / total, 4)
    return render_template('only_user_fenxi.html', list=list_gift, map_gift=map_gift, total=total, total_win=total_win, gl=gl,
                           total_cost=total_cost, total_cost_win=total_cost_win, chengben=chengben, yinkui=yinkui, max_jg=max_jg)


if __name__ == '__main__':
    fenxi(105273, '2023-02-21')