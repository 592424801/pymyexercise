# -*- coding: utf-8 -*-

import requests
import json
import hashlib
import time
import _thread
from urllib.parse import urlencode

"""
接口测试代码
Created on Tue Jul 27 12:41:39 2021
@author: Administrator
"""

token = ""
def getHtml(url, paramss, type = "post"):
    try:

        header = {'Content-Type':'application/json; charset=UTF-8',
            "app_type":"1",
                  "version":"1.21.38-test210617-1",
                  "Authorization": token}  
        # params:{uid=40076, oid=43296, key=43296, type=0, gid=43, gift_type=1, num=1, plat=android, yzt=56070fae6b37ad772ae6ed994a082038}
        # 代理
        # prox = {}

        print("================  请求地址和参数  ======================")
        print(url)
        print(paramss)
        print("========================================================")
        data = requests.post(url, data=paramss, headers=header, timeout=2)

        data.encoding = "utf-8"
        # print(data.text)
        return data.text
    except Exception as e:
        print(str(e))
        return "异常"



"""
==============================================================================
    实际用户API接口调用
==============================================================================
"""
# 登录将登录数据放在文件中
def login(phone, code="831246"): # 831246 202088
    # phone = "18782927715"
    # paramss =  '{"uid":"40026", "amount":"9800"}'
    paramss = {"code": code, "cell":phone, "did":"imei866182040064619",
               "mac":"3C:86:D1:5A:EB:13", "dev":"", "plat":"vivo_V1928A", "ver":"28_9", 
               "sys":"androidphone", "sver":"1.21.39-test210726_18", "firm":"9", 
               "mart":"android", "token":"", "ip":"192.168.0.201", "source":"and-0", 
                "lat":"30.554378",
               "lng":"104.07132", "city":"510100", "cityname":"成都市", 
               "state":"510000", "statename":"四川省"}
    
    dd = str(paramss)
    # print(dd)
    par = dd.encode("utf-8").decode("latin-1")
    data = getHtml(URLHOST+"yonghu/celllogin", par)
    print(data)
    file = open(TokenFile, "w")
    file.write(data)
    file.close()

# 验证码
def getCode(cell):
    paramss = {"cell": cell}
    text = URLHOST + "yonghu/getloginsms"+"?cell="+cell
    response = getHtml(text, "", "get")
    print(response)

# ======================================测试线程
def 循环执行房间送礼物(val):
    print(val)
    for i in range(1, 1000):
        time.sleep(1)
        # giftsend("20005", 58, 1) # 月光恋人



def 循环重复直播间送礼多线程操作():
    # 测试线程  测试线程  测试线程  测试线程  测试线程  测试线程
    # 创建两个线程
    try:
        _thread.start_new_thread(循环执行房间送礼物, ("开始执行任务Thread-1",))
        # _thread.start_new_thread(循环执行房间送礼物, ("开始执行任务Thread-2",))
    except:
        print("Error: 无法启动线程")
    while 1:
        pass


# 测试环境
URLHOST = "http://apitest.qingqingzhibo.com/api/"
ALLCODE = 202088
TokenFile = "tokenqingqing.txt"
# =====================================================================================================================
# main 主函数
# =====================================================================================================================
if __name__ == '__main__':
    userList = ["13540124221"]
    # 需要先登录才能执行下面的操作
    login(userList[0], ALLCODE)  # 登录
    循环重复直播间送礼多线程操作()
    # 循环重复直播间送礼多线程操作()






