
# 幼儿园报名数据
import requests
import json


def get_data_yey():
    url = 'https://xqzsrx.cdzk.org/school/loadSchools?schoolType=0&resBuzhao=0&schoolName='
    headers = {}
    # 从文件中读取headers信息
    with open('headers.txt', 'r') as f:
        line = f.readline()
        while line:
            if line is not None:
                # print(line.strip())
                values = line.strip().split(':')
                headers[values[0]] = values[1]
            line = f.readline()

    # for kk in headers:
    #     print(headers[kk])
    # params = {'key1': 'value1', 'key2': 'value2'}
    response = requests.get(url=url, headers=headers)
    # response.encoding = "utf-8"

    value = json.loads(response.text)
    lists = []
    map_distance = {
        3084: '6.6',
        3085: '5.0',
        3086: '3.3',
        3087: '1.7',
        3088: '3.4',
        3089: '0.7',
        3090: '3.3',
        3091: '1.3',
        3092: '2.0',
        3093: '0.5',
        3094: '3.3',
        3095: '2.0',
        3096: '0.6',
        3097: '4.5',
        3098: '3.5',
        3099: '3.3',
        3100: '4.6',
        3101: '4.0',
        3102: '2.2',
        3103: '2.3',
        3104: '0.6',
        3105: '1.3',
        3106: '1.3',
        3107: '2.9',
        3108: '2.3',
        3109: '4.6',
        3110: '1.8',
        3111: '5.6',
    }
    try:
        for v1 in value['data']['list']:
            map1 = {}
            map1['schoolId'] = v1['schoolId']
            map1['schoolName'] = v1['schoolName']
            map1['realPlanCount'] = v1['realPlanCount']
            map1['applyCount'] = v1['applyCount']
            map1['distance'] = map_distance[v1['schoolId']]
            gl_info = map1['realPlanCount'] / map1['applyCount']
            if gl_info >=1:
                gl_info = 1
            map1['gl'] = str(round(gl_info, 4) * 100)+'%'
            lists.append(map1)
        sorted_data = sorted(lists, key=lambda x: x['distance'])
        return sorted_data
    except:
        return None


if __name__ == '__main__':
    print('开始获取数据')
    data = get_data_yey()

    for v1 in data:
        print(v1)
    # print(value['data']['list'])
