from flask import Flask
from flask import render_template
from youeryuanbaoming import *
import time

start_time = time.time()
app = Flask(__name__)
html_data = ""


@app.route('/', methods=["GET", "POST"])
def hello_world():
    global html_data
    global start_time
    end_time = time.time()
    if ((end_time - start_time) > 60) or (len(html_data)==0):
        new_data = get_data_yey()
        if new_data is not None:
            html_data = new_data
            start_time = time.time()
        print('---------------更新数据---------------')

    total_num = 0
    total_num1 = 0
    for v1 in html_data:
        total_num += v1['realPlanCount']
        total_num1 += v1['applyCount']
    return render_template('index.html',
                           data=html_data,
                           update=time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(start_time)),
                           total_num=total_num,
                           total_num1=total_num1
                           )


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5001)
