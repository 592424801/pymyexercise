import time
import threading
from adb_cmd import *


# **********START 趣头条小游戏水车**********
class MyThread (threading.Thread):   # 继承父类threading.Thread
    def __init__(self, thread_id, name, counter):
        threading.Thread.__init__(self)
        self.threadID = thread_id
        self.name = name
        self.counter = counter

    # 把要执行的代码写到run函数里面 线程在创建后会直接运行run函数
    def run(self):
        click_my(self.name)


def click_my(str_name):
    count = 0
    for i in range(0, 10000):
        count += 1
        print('当前线程为: {}, 点击次数为: {}'.format(str_name, count))
        adb_click(956, 1481)


# 水车逻辑
def click_water_cart():
    arr = ['张三', '李四', '王五', '刘禅', '关羽', '张飞']
    for dd in range(0, 5):
        thread1 = MyThread(dd, arr[dd], dd)
        thread1.start()
# END===========================================点击水车


# START----------------------------------------------------------------------------------------------------趣头条小游戏
# 趣头条抽奖页面广告
def click_chou_jiang():
    for i in range(1, 8):
        adb_click(639, 1365)
        time.sleep(1.2)
        adb_click(550, 300)
    adb_click(550, 300)


# 趣头条游戏   购买道具
def gou_mai_dao_ju():
    click_list = [(844, 1722), (373, 984), (125, 1829)]
    adb_click(click_list[0][0], click_list[0][1])
    for i in range(0, 30):
        time.sleep(0.5)
        adb_click(click_list[1][0], click_list[1][1])
    adb_click(click_list[2][0], click_list[2][1])


# 土地升级
def up_level():
    # 9块地的坐标
    click_list = [(520, 1185), (271, 1048), (530, 965), (300, 853), (750, 825), (542, 708), (300, 600), (760, 600),
                  (530, 478)]
    adb_click(527, 1718)
    for i in range(0, 100):
        for j in range(0, len(click_list)):
            adb_click(click_list[j][0], click_list[j][1])
            time.sleep(0.1)
    adb_click(800, 1756)


# 偷菜每日5次
def tou_chai():
    click_list = [(877, 465), (883, 660), (880, 834), (867, 1000), (880, 1200)]
    adb_click(450, 296)
    for i in range(0, 5):
        time.sleep(1)
        adb_click(click_list[i][0], click_list[i][1])
        time.sleep(3)
        adb_click(555, 1739)
        time.sleep(3)
        adb_click(377, 1434)
        time.sleep(1)
        adb_click(162, 1765)
        time.sleep(2)
    adb_click(990, 270)


def qu_tou_tiao_game(flag):
    """
    0抽奖广告
    1购买道具
    2点击水车
    3升级土地
    4偷菜
    """

    if flag == 0:
        # 抽奖广告
        click_chou_jiang()
    elif flag == 1:
        # 购买道具
        gou_mai_dao_ju()
    elif flag == 2:
        # 点击水车
        click_water_cart()
    elif flag == 3:
        # 升级土地
        up_level()
    elif flag == 4:
        # 偷菜
        tou_chai()


# END------------------------------------------------------------------------------------------------------趣头条小游戏
