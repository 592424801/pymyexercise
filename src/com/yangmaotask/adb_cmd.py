import os


# XIAO_MI_DEVICE = '-s 41af34c7'
import time

# XIAO_MI_DEVICE = '-s 192.168.31.112:8888'
XIAO_MI_DEVICE = '-s 41af34c7'
# HUA_WEI_DEVICE = '-s H6D5T15525001198'
HUA_WEI_DEVICE = '-s SJE5T17503011219'
HUA_WEI_DEVICE1 = '-s emulator-5554'
HUA_WEI_DEVICE2 = '-s H6D5T15525001198'

device_id = []


# 设置执行设备
def set_device(device):
    global device_id
    device_id = device


# 执行adb命令
def adb_ran(cmd):
    # adb_cmd = cmd.replace('-device', XIAO_MI_DEVICE)
    # if device_id == 0:
    #     adb_cmd = cmd.replace('-device', XIAO_MI_DEVICE)        # 小米
    # elif device_id == 2:
    #     adb_cmd = cmd.replace('-device', HUA_WEI_DEVICE1)  # 模拟器1
    # elif device_id == 3:
    #     adb_cmd = cmd.replace('-device', HUA_WEI_DEVICE2)  # 模拟器1
    # else:
    #     adb_cmd = cmd.replace('-device', HUA_WEI_DEVICE)         # 华为
    # # print(str(time.strftime('%H:%M:%S', time.localtime(time.time()))), adb_cmd, device_id)
    # os.system(adb_cmd)
    for item in device_id:
        temp = cmd
        temp = temp.replace('-device', item)
        os.system(temp)


# 点击事件
def adb_click(x, y):
    cmd = "adb -device shell input tap {x1} {y1}".format(
        x1=x,
        y1=y
    )
    adb_ran(cmd)


# 屏幕上滑动
def adb_swipe(x1, y1, x2, y2):
    cmd = "adb -device shell input swipe {} {} {} {}".format(
        x1,
        y1,
        x2,
        y2
    )
    adb_ran(cmd)


# 连续滑动
def adb_swipes(x1, y1, x2, y2, x3, y3, x4, y4, x5, y5):
    cmd = "adb -device shell input touchscreen swipe {} {} {} {} {} {} {} {} {} {}".format(
        x1,
        y1,
        x2,
        y2,
        x3,
        y3,
        x4,
        y4,
        x5,
        y5
    )
    adb_ran(cmd)


# 返回
def adb_back():
    cmd = "adb -device shell input keyevent 4"
    adb_ran(cmd)


# 返回首页
def adb_home():
    cmd = "adb -device shell input keyevent 3"
    adb_ran(cmd)


# 菜单键
def adb_menu():
    cmd = "adb -device shell input keyevent 1"
    adb_ran(cmd)


# 亮屏
def adb_bright_view():
    cmd = "adb -device shell input keyevent 224"
    adb_ran(cmd)


# 息屏
def adb_xi_screen():
    cmd = "adb -device shell input keyevent 223"
    adb_ran(cmd)


# 启动activity
def adb_open_activity(activity):
    cmd = "adb -device shell am start -n {}".format(
        activity
    )
    adb_ran(cmd)


# 截图 上传到电脑
def adb_jietu():
    # 截图
    cmd = "adb -device shell /system/bin/screencap -p /sdcard/screenshot.png"
    adb_ran(cmd)
    time.sleep(3)
    # 上传到手机
    cmd1 = "adb -device pull /sdcard/screenshot.png d:/photo/screenshot.png"
    adb_ran(cmd1)