import time
import random
from adb_cmd import *


# 趣头条 阅读文章 阅读视频(执行命令)
def swipe(isup):
    if isup > 4:
        adb_swipe(100, 810, 100, 200)
    else:
        adb_swipe(100, 300, 100, 510)


# 趣头条首页文章阅读
def homepage_read():
    # 点击首页
    time.sleep(1)
    adb_click(124, 1711)
    for k in range(0, 1000):
        # 点击一个列表项
        adb_click(630, 774)
        time.sleep(1)
        print("阅读下一篇内容 {}, 执行次数为：{}".format(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()), k))
        for i in range(0, random.randint(5, 15)):
            # 循环滑动10次
            adb_swipe(100, 990, 100, 600)
            time.sleep(random.randint(2, 4))
        adb_back()
        time.sleep(1)
        # 滑动一下列表好点击下一项
        adb_swipe(730, 1142, 756, 796)
        time.sleep(2)


# 趣头条视频阅读
def click_qutoutiao():
    """趣头条 阅读文章"""
    print('开始执行趣头条阅读任务')
    for i in range(0, 9999):
        swipe(15)
        time.sleep(random.randint(9, 12))
        print('执行次数：{}, 执行时间:{} '.format(str(i), time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())))


# 打开趣头条app
def go_q():
    # 去首页
    adb_home()
    time.sleep(1)
    # 点击趣头条
    adb_click(200, 1054)
    time.sleep(12)


# 去头条第一次打开
def go_q_open():
    # 打卡领金币
    time.sleep(1)
    adb_click(580, 1414)
    # 关闭打卡领金币
    time.sleep(1)
    adb_click(555, 1729)
    # 关闭升级弹窗
    time.sleep(1)
    adb_click(888, 662)
    # 点击首页领取金币
    time.sleep(1)
    adb_click(986, 154)

    # 签到领金币任务页面
    time.sleep(1)
    adb_click(575, 1381)
    # 关闭签到领金币
    time.sleep(1)
    adb_click(548, 1649)

    # 点击首页
    time.sleep(1)
    adb_click(124, 1711)


# =====================================================================================================================
# 快手
def go_ks():
    adb_home()
    time.sleep(1)
    # 点击快手
    adb_click(429, 1054)
    time.sleep(14)


# 快手第一次打开
def go_ks_open():
    # 青少年模式弹窗关闭
    adb_click(600, 1585)
    time.sleep(1)
    # 面对面邀请弹窗关闭
    adb_click(153, 351)
    time.sleep(1)

    # 点击签到
    time.sleep(1)
    adb_click(122, 435)
    time.sleep(4)
    # 立即签到
    adb_click(576, 1314)
    time.sleep(1)
    adb_click(547, 1616)
    # 点击返回
    adb_back()


# =====================================================================================================================
# 快看点
def go_kkd():
    adb_home()
    time.sleep(1)
    # 点击快手
    adb_click(670, 1054)
    time.sleep(15)
    # 点击我的
    adb_click(879, 1705)
    time.sleep(8)
    # 点击首页
    adb_click(107, 1705)


# 小米淘宝618
def go_mi():
    for i in range(0, 10):
        time.sleep(3)

        # 淘宝循环任务
        adb_click(900, 1288)
        # 支付宝，知道了
        # adb_click(550, 1380)
        # 支付宝任务
        # adb_click(892, 623)

        time.sleep(5)
        for j in range(0, 6):
            adb_swipe(100, 990, 100, 600)
            time.sleep(2)
        time.sleep(5)
        adb_back()



