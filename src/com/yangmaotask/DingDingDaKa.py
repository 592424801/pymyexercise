from adb_cmd import *
from gold import *
from adb_cmd import *
import time
import datetime  # 导入datetime模块
import threading  # 导入threading模块


def run():  # 定义方法
    print(datetime.datetime.now())  # 输出当前时间
    now = datetime.datetime.now()
    suffix = f'{now.year:04d}{now.month:02d}{now.day:02d}{now.hour:02d}{now.minute:02d}{now.second:02d}'
    print(suffix)  # 因为我是用它作为后缀用的，所以就这样写了

    if now.hour >= 19:
        main()
    else:
        timer = threading.Timer(10, run)  # 每秒运行
        timer.start()  # 执行方法


def main():
    set_device(0)
    adb_home()
    # 亮屏
    adb_bright_view()
    time.sleep(2)
    # 向上滑动
    adb_swipe(100, 500, 100, 50)
    time.sleep(2)
    # adb_swipe(250, 1094, 250, 1379)
    # adb_swipe(250, 1379, 250, 1666)
    # adb_swipe(250, 1666, 550, 1666)
    # adb_swipe(550, 1666, 833, 1666)
    # com.tencent.wework/.launch.WwMainActivity
    # adb_open_activity("com.tencent.wework/.launch.WwMainActivity")

    adb_home()
    time.sleep(2)
    adb_home()
    time.sleep(2)
    adb_swipe(900, 1500, 100, 1800)
    time.sleep(2)
    adb_click(150, 888)
    time.sleep(2)
    adb_click(531, 1198)
    time.sleep(2)


if __name__ == '__main__':
    # main()
    t1 = threading.Timer(1, function=run)  # 创建定时器
    t1.start()  # 开始执行线程