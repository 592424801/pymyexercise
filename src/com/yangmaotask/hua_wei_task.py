from adb_cmd import *
from gold import *
import aircv as ac
import re


def exec_cmd(cmd):
    try:
        print(cmd)
        cmd = os.popen(cmd)
        cmd_result = cmd.read()
        cmd.close()
        return cmd_result
    except:
        print("执行命令报错了")
    return "------------"


def get_device_ip(content):
    math_obj = re.search(r'inet\s(\d+\.\d+\.\d+\.\d+).*?wlan0', content)
    if math_obj and math_obj.group(1):
        return math_obj.group(1)
    return None

def adb_wifi(phone_type):
    if phone_type == 'xiaomi':
        # exec_cmd("adb -s 66f03f61 tcpip 8888")
        print(exec_cmd('adb connect 192.168.31.112:8888'))
        print(exec_cmd('adb connect 192.168.31.147:8888'))
        # print(exec_cmd('adb connect 192.168.31.21:8888'))
    else:
        exec_cmd('adb tcpip 8888')
        time.sleep(3)
        result = exec_cmd('adb shell ip addr show wlan0')
        print(result)
        ip = get_device_ip(result)
        print(ip)
        exec_cmd('adb connect '+str(ip) + ':8888')

imgobj = r'D:\photo\screenshot.png'
# find_template(原始图像imsrc，待查找的图⽚imobj，最低相似度confidence)
# confidence：匹配相似率
# rectangle：匹配图⽚在原始图像上四边形的坐标
# result：匹配图⽚在原始图⽚上的中⼼坐标点，也就是我们要找的点击点
def matchImg(imgsrc, confidencevalue=0.5):
    imsrc = ac.imread(imgsrc)
    imobj = ac.imread(imgobj)
    match_result = ac.find_template(imsrc, imobj, confidencevalue)
    # {'confidence': 0.5435812473297119, 'rectangle': ((394, 384), (394, 416), (450, 384), (450, 416)), 'result': (422.0, 400.0)}
    if match_result is not None:
        # 0为⾼，1为宽
        match_result['shape'] = (imsrc.shape[1], imsrc.shape[0])
        print(match_result)
        return True
    return False


def test():
    # adb_jietu()
    # ac.imread('d:\\photo\\srctt.png')  # 打开查找页
    imgsrc = r'D:\photo\tag1.png'
    # ac.imread('d:\\photo\\screenshot.png')  # 打开待识别的图片
    # imgobj = r'D:\photo\screenshot.png'
    dd = matchImg(imgsrc, 0.6)
    print(dd)


# 快手看直播
def go_ks_live(dd):
    for j in range(0, 31):
        time.sleep(2)
        if j % 2 == 1:
            adb_swipe(100, 1300, 100, 676)
            time.sleep(1)
            # 点击直播间
            adb_click(312, 647)
        else:
            adb_click(812, 647)
        if dd == '快手':
            time.sleep(33)
        else:
            time.sleep(66)
        # 快手需要，快手急速版不需要
        if dd == '快手':
            adb_click(979, 354)
            time.sleep(2)
            # 清理页面弹窗
            adb_click(40, 700)
            time.sleep(2)
            adb_click(850, 341)
            time.sleep(1)
            adb_click(40, 700)
            time.sleep(1)
        # 退出直播间
        print(str(time.strftime('%H:%M:%S', time.localtime(time.time())))+"点击退出")
        # 清理页面弹窗
        time.sleep(1)
        adb_click(40, 500)
        time.sleep(1)
        adb_click(40, 500)
        # 右上角关闭按钮
        adb_click(1015, 156)
        time.sleep(2)
        # 切图获取手机图片，判断手机图片是否有打开别的app
        adb_jietu()
        time.sleep(6)
        if matchImg(r'D:\photo\tag3.png', 0.69):
            adb_click(570, 1400)
        print(str(time.strftime('%H:%M:%S', time.localtime(time.time())))+"   执行次数 -----> ", j)


def main(flag, task):

    if flag == 1:
        # 打开趣头条app
        # go_q()
        # go_q_open()
        # 首页文章阅读
        homepage_read()

        # 滑动阅读趣头条视频
        # click_qutoutiao()
        # 趣头条游戏   0抽奖广告 1购买道具 2点击水车 3升级土地 4偷菜
        # qu_tou_tiao_game(2)
    elif flag == 2:
        # 快手
        go_ks()
        # go_ks_open()
        click_qutoutiao()
    elif flag == 3:
        # 快看点
        go_kkd()
        homepage_read()
    elif flag == 4:
        # 淘宝618活动
        go_mi()
        # 看视频
        # click_qutoutiao()
    elif flag == 5:
        if task == '看视频':
            # 滑动浏览视频
            for j in range(1, 400):
                print(str(time.strftime('%H:%M:%S', time.localtime(time.time())))+"   执行次数 -----> ", j)
                adb_swipe(100, 1500, 100, 376)
                time.sleep(random.randint(20, 36))
            # 滑动浏览视频end
        elif task == '逛街':
            # 逛街
            for j in range(1, 400):
                adb_swipe(100, 1300, 100, 676)
                time.sleep(4)
                print(str(time.strftime('%H:%M:%S', time.localtime(time.time())))+"   执行次数 -----> ", j)
            # 逛街end
        elif task == '转盘':
            # 转盘
            for i in range(1, 30):
                print(str(time.strftime('%H:%M:%S', time.localtime(time.time())))+"   执行次数 -----> ", i)
                # 抽奖按钮
                adb_click(582, 1780)
                time.sleep(31)
                # 关闭广告视频
                adb_click(435, 177)
                time.sleep(15)
                # 关闭转盘抽中弹窗
                adb_click(536, 1709)
                time.sleep(3)
            # 转盘end
        elif task == '看广告':
            # 看视频
            for i in range(1, 21):
                adb_click(914, 470)
                time.sleep(27)
                adb_click(433, 175)
                time.sleep(1)
                # 切图获取手机图片，判断手机图片是否有打开别的app
                adb_jietu()
                time.sleep(6)
                if test():
                    adb_click(550, 1538)
                print(str(time.strftime('%H:%M:%S', time.localtime(time.time())))+"   执行次数 -----> ", i)
            # 看视频END
        elif task == '抖音gg':
            for i in range(1, 10):
                if matchImg(r'D:\photo\taglj.png', 0.5):
                    adb_click(570, 1318)
                else:
                    adb_click(936, 374)
                time.sleep(33)
                adb_click(994, 137)
                time.sleep(1)
                # 切图获取手机图片，判断手机图片是否有打开别的app
                adb_jietu()
                time.sleep(6)
                if matchImg(r'D:\photo\tagfh.png', 0.5):
                    adb_click(77, 206)
                    time.sleep(2)
                    # 切图获取手机图片，判断手机图片是否有打开别的app
                    adb_jietu()
                    time.sleep(6)


if __name__ == '__main__':
    # 0 小米 1华为
    set_device(0)
    # adb_wifi('xiaomi')

    # 获取手机切图
    # adb_jietu()
    # 比较这两张图的相似度
    # print(matchImg(r'D:\photo\tag3.png', 0.2))

    # 直播  看视频 转盘  看广告 抖音gg 逛街
    # main(5, '看视频')
    # main(5, '逛街')
    # main(5, '转盘')
    main(5, '看广告')
    # 直播
    # go_ks_live('快手极速')


