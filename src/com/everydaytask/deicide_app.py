import time

from flask import Flask, Response
from flask import render_template
from flask import request
from day_task_mysql import *
from mysql_notepad import *

app = Flask(__name__)

# ======================================================================================================================
# 任务列表api
@app.route('/add', methods=['POST', 'GET'])
def add():
    if request.method == 'POST':
        content = request.form['content']
        add_task(content)
        return app_main()
    return render_template('h_404.html')


@app.route('/updata', methods=['POST', 'GET'])
def updata():
    if request.method == 'POST':
        id = request.form['id']
        change_info(id)
        return app_main()
    return render_template('h_404.html')


@app.route('/updatanum', methods=['POST', 'GET'])
def updata_num():
    if request.method == 'POST':
        id = request.form['id']
        num = request.form['num']
        print("点赞====", id, num )
        change_info_num(id, num)
        return app_main()
    return render_template('h_404.html')


@app.route('/del', methods=['POST', 'GET'])
def delete_info():
    if request.method == 'POST':
        id = request.form['id']
        del_task_info(id)
        return app_main()
    return render_template('h_404.html')


@app.route('/', methods=['POST', 'GET'])
def app_main():
    try:
        time = request.values.get("liststatus")
        data_s = []
        if time == None or time == '0':
            data_s = find_all_ing()
        else:
            data_s = find_all()
        data = []
        for item in data_s:
            data_item = []
            data_item.append(item['id'])
            data_item.append(item['content'])
            data_item.append(item['status'])
            data_item.append(item['time'])
            data_item.append(item['num'])
            data_item.append(outTimeChinese(item['newtime']))
            data.append(data_item)

        time_list = []
        # 日期列表显示出来
        for day in range(0, 13):
            time_list.append((datetime.datetime.now() + datetime.timedelta(days=-day)).strftime('%Y-%m-%d'))
        return render_template('index.html', list=data, time_list=time_list, nowtime=time)
    except Exception as e:
        print('=================error  ' + str(e))
        return render_template('h_404.html')
# END----------------------------------------------------------------------------------------------------------任务------


# ======================================================================================================================
# 记事本相关功能api
@app.route('/notepad')
def notepad_main():
    id = request.values.get("id")
    type = request.values.get("type")
    item_map = {}
    if id != None and id != 0 and len(id)>0:
        item_map = find_notepad_id(id)

    if type != None and len(type) > 0:
        list_data = find_notepad_all(type)
    else:
        type = ""
        list_data = find_notepad_all("")
    list_data.reverse()

    return render_template('notepad_index.html', list=list_data, item_map=item_map, type=type)


@app.route('/notepad_updata', methods=['POST', 'GET'])
def notepad_updata():
    if request.method == 'POST':
        id = request.form['id']
        title = request.form['title']
        content = request.form['content']
        type = request.form['type']
        change_notepad_info(title, content, id, type)
        return notepad_main()
    return render_template('h_404.html')
# ---------------------------------------------------------------------------------记事本api end

# ===============================================================================================================日期工具
def outTimeChinese(input_date):
    try:
        # 获取今天的日期
        today = datetime.date.today()

        # 输入日期
        year, month, day = map(int, input_date.split("-"))
        input_date = datetime.date(year, month, day)

        # 计算日期差值
        delta = today - input_date
        days_diff = delta.days

        # 输出结果
        if days_diff == 0:
            return '今天'
        elif days_diff == 1:
            return "昨天"
        elif days_diff == 2:
            return "前天"
        elif days_diff > 2:
            return f"{days_diff}天前"
        else:
            return "未完成"
    except:
        return "--"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)