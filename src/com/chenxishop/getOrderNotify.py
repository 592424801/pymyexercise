
# 获取新的订单，并且获取到新的订单推送给微信用户
import requests
import json
import hashlib
import time
import _thread
from urllib.parse import urlencode


def getHtml(url, paramss, type="post", userMap={}):
    try:
        header = {'Content-Type': 'application/json; charset=UTF-8',
                  "app_type": "1",
                  "version": "1.21.38-test210617-1"}
        if ("null" != userMap.get("token", "null")):
            header["Authorization"] = userMap["token"]
        if ("post" == type):
            if ("null" != userMap.get("yzt", "null")):
                url += userMap["yzt"]
            print("================  请求地址和参数  ======================")
            print(url)
            print(paramss)
            print("========================================================")
            data = requests.post(url, data=paramss, headers=header, timeout=2)
        else:
            data = requests.get(url, data=paramss, headers=header, timeout=2)
        data.encoding = "utf-8"
        # print(data.text)
        return data.text
    except Exception as e:
        print(str(e))
        return "异常"


if __name__ == '__main__':
    url = "http://jbblog.liveshop.vip/prod-api/account/login?username=zhanghong123&password=zhanghongxuhuan.5"# username=zhanghong123&password=zhanghongxuhuan.5
    parames = {}
    print(getHtml(url, parames))