from flask import Flask, request, jsonify, render_template
# import openai
import os

# Set up OpenAI API credentials
# openai.api_key = os.environ.get("OPENAI_API_KEY")

# Initialize Flask app
app = Flask(__name__)


# Define endpoint for chat messages
# @app.route('/chat', methods=['POST'])
# def chat():
#     # Get message from request body
#     message = request.json['message']
#
#     # Call OpenAI's GPT-3 API to generate a response
#     response = openai.Completion.create(
#         engine="davinci",
#         prompt=message,
#         max_tokens=60,
#         n=1,
#         stop=None,
#         temperature=0.5
#     )
#
#     # Return response as a JSON object
#     return jsonify({'message': response.choices[0].text.strip()})

@app.route('/')
def hello():
    return render_template('index.html')


if __name__ == '__main__':
    app.run()
