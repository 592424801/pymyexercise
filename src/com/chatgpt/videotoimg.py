import cv2
import os

# 可以把视频一帧一帧的导出到文件夹，然后把每一帧重新生成视频

def my_fun():
    cap = cv2.VideoCapture('d:\\test\\video.mp4')
    i = 0
    while (cap.isOpened()):
        # 读取一帧
        ret, frame = cap.read()

        if ret == False:
            break

        # 保存图像
        cv2.imwrite('d:\\test\\img\\frame{}.jpg'.format(i), frame)

        i += 1
    cap.release()


# 图片生成视频
def img_to_video():
    img_folder = 'd:\\test\\img'
    video_name = 'd:\\test\\output_video.mp4'
    fps = 30.0  # 视频的帧率

    images = [img for img in os.listdir(img_folder) if img.endswith(".jpg")]

    print(images)
    images = []
    for i in range(0,2901):
        images.append('frame{}.jpg'.format(i))
    print(images)

    frame = cv2.imread(os.path.join(img_folder, images[0]))
    height, width, layers = frame.shape

    video = cv2.VideoWriter(video_name, 0, fps, (width, height))

    for image in images:
        img = cv2.imread(os.path.join(img_folder,image))
        video.write(img)
    cv2.destroyAllWindows()
    video.release()


if __name__ == "__main__":
    # my_fun()
    img_to_video()