from PIL import Image
import os, shutil

# 这个工具对图片进行压缩

# 图片压缩批处理
def compressImage(srcPath, dstPath):
    for filename in os.listdir(srcPath):
        # 如果不存在目的目录则创建一个，保持层级结构
        if not os.path.exists(dstPath):
            os.makedirs(dstPath)

        # 拼接完整的文件或文件夹路径
        srcFile = os.path.join(srcPath, filename)
        dstFile = os.path.join(dstPath, filename)

        quality = 40
        step = 10

        o_size = get_size(srcFile)
        # 如果是文件就处理
        print(o_size)
        if os.path.isfile(srcFile):
            print(srcFile)
            if o_size < 100:
                im = Image.open(srcFile)
                im.save(dstFile, quality=quality)
                return
            try:
                while o_size > 100:
                    im = Image.open(srcFile)
                    im.save(dstFile, quality=quality)
                    if quality - step < 0:
                        break
                    quality -= step
                    o_size = get_size(dstFile)
            except Exception as e:
                print(dstFile + "失败！" +str(e))

        # 如果是文件夹就递归
        if os.path.isdir(srcFile):
            compressImage(srcFile, dstFile)


# 获取文件大小
def get_size(file):
    # 获取文件大小:KB
    size = os.path.getsize(file)
    return size / 1024


if __name__ == '__main__':
    # file1 = r"./原图"
    file2 = r'./原图'
    file3 = r"./可以上传到服务器的压缩后的图片"
    # 判断文件夹是否存在，不存在则创建
    # if not os.path.exists(file1):
    #     os.makedirs(file1)
    # 判断文件夹是否存在，不存在则创建
    if not os.path.exists(file2):
        os.makedirs(file2)
    # 判断文件夹是否存在，不存在则创建
    if not os.path.exists(file3):
        os.makedirs(file3)

    # 遍历待加入图片
    # path = os.walk(file1)
    # for root, dirs, files in path:
    #     for f in files:
    #         shutil.move(os.path.join(root, f), os.path.join(file2, f))  # 移动文件
    #
    # # 遍历删除压缩图片
    # path = os.walk(file3)
    # for root, dirs, files in path:
    #     for f in files:
    #         os.remove(os.path.join(root, f))

    # 遍历压缩图片
    compressImage(file2, file3)
