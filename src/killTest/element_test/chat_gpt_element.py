from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys


# https://zhuanlan.zhihu.com/p/623536553
def test():
    # 禁止浏览器自动关闭
    option = webdriver.ChromeOptions()
    option.add_experimental_option("detach", True)
    option.binary_location = 'D:/MySoft/chrome-win/chrome.exe'  # 请替换为实际的Chrome路径
    option.add_argument('--no-sandbox')
    option.add_argument('--disable-dev-shm-usage')
    option.add_argument('--disable-gpu')

    # 驱动器
    chrome_driver_path = 'C:/Users/Administrator/Downloads/chromedriver_win32/chromedriver.exe'
    chrome_service = webdriver.chrome.service.Service(chrome_driver_path)
    # 创建Chrome浏览器实例
    driver = webdriver.Chrome(options=option, service=chrome_service)
    # 打开地址
    driver.get("https://www.baidu.com")
    # 全屏
    # driver.maximize_window()

    # 定位元素
    driver.find_element(By.ID, "kw").send_keys("selenium")

    # 鼠标悬浮操作
    # ActionChains(driver).move_to_element(
    #     driver.find_element(By.XPATH, "/html/body/div[1]/div[1]/div[3]/div/a")).perform()

    # 输入组合键 Ctrl+a,全选输入框内容
    driver.find_element(By.ID, "kw").send_keys(Keys.CONTROL, "a")

    # 输入组合键 Ctrl+x，剪切输入框内容
    info = driver.find_element(By.ID, "kw").send_keys(Keys.CONTROL, "x")
    print(info)

    # search_btn = driver.find_element(By.ID, "kw")
    # if search_btn:
    #     search_btn.click()


if __name__ == '__main__':
    test()