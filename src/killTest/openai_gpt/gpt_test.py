import openai
# openai key
my_key = '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'

def test():

    prompt = "Translate the following English text to French: '{}'"
    user_input = input("Enter the text you want to translate: ")

    openai.api_key = my_key
    response = openai.chat.completions.create(
        model="gpt-3.5-turbo-1106",
        response_format={"type": "json_object"},
        messages=[
            {"role": "system", "content": "You are a helpful assistant designed to output JSON."},
            {"role": "user", "content": "Who won the world series in 2020?"}
        ]
    )
    print(response.choices[0].message.content)


if __name__ == '__main__':
    test()